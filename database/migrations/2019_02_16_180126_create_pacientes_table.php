<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {

            $table->increments('id');
            $table->enum('nacionalidad', ['V','E']);
            $table->integer('cedula');
            $table->string('nombres');
            $table->string('apellidos');
            $table->date('fec_nac');
            $table->string('telefono');
            $table->string('correo');
            $table->text('residencia');
            $table->string('municipio');
            $table->integer('status')->default('1');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
