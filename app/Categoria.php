<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Medicamento;

class Categoria extends Model
{
   
   protected $table = 'categorias';

   protected $fillable = ['name'];

   public function Medicamentos()
   {
   	
    return $this->hasMany(Medicamento::class);

   }


}
