<?php

namespace App\Http\Controllers;

use Illuminate\support\Facades\Session;
use Illuminate\Http\Request;
use App\Paciente;
use App\Medicamento;
use Barryvdh\DomPDF\Facade as PDF;

class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         $pacientes=Paciente::all();

         return view('pacientes.index', compact('pacientes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['nacionalidad' => 'required', 'cedula' => 'required|unique:pacientes', 'nombres' => 'required', 'apellidos' => 'required|max:20', 'genero' => 'required','fec_nac' => 'required','telefono' => 'required|numeric', 'correo' => 'required','residencia' => 'required', 'municipio' => 'required'],['required' => 'Campo Requerido', 'unique' => 'Ya este paciente esta registrado!']);
        
        $paciente = new Paciente();
        $paciente->nacionalidad = $request->nacionalidad;
        $paciente->cedula = $request->cedula;
        $paciente->nombres = $request->nombres;
        $paciente->apellidos = $request->apellidos;
        $paciente->genero = $request->genero;
        $paciente->fec_nac = $request->fec_nac;
        $paciente->telefono = $request->telefono;
        $paciente->correo = $request->correo;
        $paciente->residencia = $request->residencia;
        $paciente->municipio = $request->municipio;
        $paciente->save();

        Session::flash('message','Se ha registrado exitosamente al Paciente: '.$request->nombres.' '.$request->apellidos.'!');
        Session::flash('class','success');

        return redirect('/pacientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::find($id);
        $medicamento = Medicamento::all();
        return view('pacientes.show', compact('paciente', 'medicamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

        public function historia($id)
    {

          $paciente = Paciente::find($id);
          return $paciente;
        
    }

    public function historiaPDF($id)
    {
        $paciente = Paciente::find($id);
        
        $pdf = PDF::loadView('pdf.paciente', compact('paciente'));

        return $pdf->stream('listado');
    }

    public function list()
    {
        $pacientes = Paciente::all();
        return view('pacientes.list', compact('pacientes'));
    }

    public function suspender($id,$status)
    {
          
        $sts = $status;  

        $paciente = Paciente::find($id);
        $paciente->status = $status;
        $paciente->save();

        if ($sts == 1) {

         Session::flash('message','Se ha Suspendido al Paciente: '.$paciente->nombres.' '.$paciente->apellidos.'!');
         Session::flash('class','info');

        }else{

          Session::flash('message','Se ha Activado al Paciente: '.$paciente->nombres.' '.$paciente->apellidos.'!');
          Session::flash('class','success');
        }
        
        
    }
}
