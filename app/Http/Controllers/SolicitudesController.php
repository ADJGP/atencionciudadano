<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Session;
use App\Solicitud;
use App\Paciente;
use App\Medicamento;
use App\Categoria;

class SolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = Solicitud::all();
        return view('solicitudes.index', compact('solicitudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::where('status',1)->get();
        return view('solicitudes.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $request->validate(['cedula' => 'required', 'cat_m' => 'required', 'cantidad_e' => 'required', 'detalle' => 'required'],['required' => 'Campo Requerido']);

         $solicitud = new Solicitud();
         $solicitud->paciente_id = $request->paciente_id;
         $solicitud->medicamento_id = $request->medicamento_id;
         $solicitud->cantidad = $request->cantidad_e;
         $solicitud->detalle = $request->detalle;
         $solicitud->save();

         $medicamento = Medicamento::find($request->medicamento_id);
         $medicamento->existencia = ($medicamento->existencia - $request->cantidad_e);
         $medicamento->save();

        Session::flash('message','Se ha procesado la solicitud!');
        Session::flash('class','success');

        return redirect('/solicitudes');


        //return dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function buscarPaciente($ced)
    {
        $paciente = Paciente::where('cedula',$ced)->get();
        $result = count($paciente);

        if ($result>0) {

          return $paciente;

        }else{

          return $result;

        }

        
    }

   public function traer_m($id)
    {
        $medicamentos = Medicamento::where('categoria_id',$id)->get();

        return $medicamentos;
    }

}
