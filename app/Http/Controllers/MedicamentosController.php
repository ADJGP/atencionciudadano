<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Medicamento;
use App\Movimiento;
use Illuminate\Support\Facades\Session;

class MedicamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::where('status',1)->get();
        $medicamentos = Medicamento::where('status',1)->get();
        return view('medicamentos.index', compact('categorias','medicamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medicamentos = Medicamento::all();
        return view('medicamentos.almacen', compact('medicamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['categoria_id' => 'required', 'name' => 'required|min:4|max:20|unique:medicamentos', 'existencia' => 'required'], ['categoria_id.required' => 'Categoria es Obligatoria', 'name.required' => 'Nombre es Obligatorio', 'name.min' => 'Longitud Minima 4 Caracteres', 'name.max' => 'Longitud Maxima 20 Caracteres', 'name.unique' => 'Este medicamento ya existe en almacen', 'existencia.required' => 'Cantidad de Existencia es Obligatoria']);

        $medicamento = new Medicamento();
        $medicamento->categoria_id = $request->categoria_id;
        $medicamento->name = $request->name;
        $medicamento->descripcion = $request->descripcion;
        $medicamento->existencia = $request->existencia;
        $medicamento->save();

        $Categoria = Categoria::find($request->categoria_id);

        $movimiento = new Movimiento();
        $movimiento->medicamento_id = $medicamento->id;
        $movimiento->tipo_mov = 'I';
        $movimiento->detalle_mov = 'Ingreso del Medicamento: '.$request->name.' en la Categoria: '.$Categoria->name;
        $movimiento->cantidad = $request->existencia;
        $movimiento->save();

        Session::flash('message','Se ha creado el Medicamento: '.$request->name.'!');
        Session::flash('class','success');

        return redirect('/medicamentos');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Categoria::where('status',1)->get();
        $medicamento = Medicamento::find($id);

        return view('medicamentos.edit', compact('medicamento','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$request->validate(['existencia' => 'required']);

        $medicamento = Medicamento::find($id);
        $name = $medicamento->name;
        $medicamento->existencia = ($medicamento->existencia+$request->existencia);
        $medicamento->save();

        $movimiento = new Movimiento();
        $movimiento->medicamento_id = $medicamento->id;
        $movimiento->tipo_mov = 'E';
        $movimiento->detalle_mov = 'Recepcion del Medicamento: '.$medicamento->name.' en la Categoria: '.$medicamento->Categoria->name;
        $movimiento->cantidad = $request->existencia;
        $movimiento->save();

        Session::flash('message','Se ha recargo el stock medicamento: '.$name.'!');
        Session::flash('class','success');
        
        return redirect('/medicamentos/create');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function desactivar($id,$status)
    {
        
        $medicamento = Medicamento::find($id);
        $medicamento->status = $status;
        $medicamento->save();


        //return $id.'aa'.$status;

    }

    public function activar($id,$status)
    {
        $medicamento = Medicamento::find($id);
        $medicamento->status = $status;
        $medicamento->save();
    }

    public function salida(Request $request, $id)
    {
        
        $medicamento = Medicamento::find($id);
        $name = $medicamento->name;
        $medicamento->existencia = ($medicamento->existencia-$request->cantidad);
        $medicamento->save();

        $movimiento = new Movimiento();
        $movimiento->medicamento_id = $medicamento->id;
        $movimiento->tipo_mov = 'S';
        $movimiento->detalle_mov = 'Salida del Medicamento: '.$medicamento->name.' en la Categoria: '.$medicamento->Categoria->name;
        $movimiento->cantidad = $request->cantidad;
        $movimiento->save();

        Session::flash('message','Se ha procesado una Salida de Almacen del medicamento: '.$name.'!');
        Session::flash('class','success');
        
        return redirect('/medicamentos/create');
     
    }
}
