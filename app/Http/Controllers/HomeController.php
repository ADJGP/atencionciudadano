<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicamento;
use App\Categoria;
use App\Paciente;
use App\Solicitud;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pacientes = Paciente::all();
        $totalP = count($pacientes);

        $medicamentos = Medicamento::all();
        $totalM = count($medicamentos);

        $solicitudes = Solicitud::all();
        $totalS = count($solicitudes);

        return view('home', compact('totalP','totalM','totalS'));
    }
}
