<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Session;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::where('status',1)->get();
        $categoriasD = Categoria::where('status',0)->get();

        return view('categorias.index', compact('categorias','categoriasD'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $request->validate(['name' => 'required|min:4|max:20|unique:categorias'],['name.required' => 'El nombre es Obligatorio', 'name.min' => 'Longitud Minima 4 Caracteres', 'name.max' => 'Longitud Maxima de 20 Caracteres', 'name.unique' => 'Ya esta categoria existe, verifique los listados.']);
       
        $category = new Categoria();
        $category->name = $request->name;
        $category->save();

        Session::flash('message','Se ha creado un registro.!');
        Session::flash('class','success');

        return redirect('/categorias');

 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        $name = $categoria->name;
        $categoria->status = '0';
        $categoria->save();

        Session::flash('message','Se ha Eliminado: '.$name);
        Session::flash('class','success');

    }

        public function restaurar($id)
    {
        $categoria = Categoria::find($id);
        $name = $categoria->name;
        $categoria->status = '1';
        $categoria->save();

        Session::flash('message','Se Restauro: '.$name);
        Session::flash('class','success');

    }

}
