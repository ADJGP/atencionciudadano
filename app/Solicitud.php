<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Paciente;
use App\Medicamento;

class Solicitud extends Model
{
    protected $table = 'solicituds';

    protected $fillable = ['paciente_id', 'medicamento_id', 'cantidad', 'detalle'];

    public function Paciente()
    {
    	return $this->belongsTo(Paciente::class);
    }

    public function buscarMedicamento($id){

       $medicamento = Medicamento::find($id);

       return $medicamento->name;

    }
}
