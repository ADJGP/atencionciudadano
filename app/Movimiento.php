<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Medicamento;

class Movimiento extends Model
{
    protected $table = 'movimientos';

    protected $fillable = ['tipo_mov','detalle_mov','medicamento_id','cantidad'];

    public function Medicamentos()
    {
    	
        return $this->hasMany(Medicamento::class);
    }
}
