<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Solicitud;

class Paciente extends Model
{
    protected $table = 'pacientes';

    protected $fillable = ['nacionalidad','cedula','nombres','apellidos','fec_nac','genero','telefono','correo','residencia','municipio'];

    public function Solicitudes()
    {
    	return $this->hasMany(Solicitud::class);
    }
}
