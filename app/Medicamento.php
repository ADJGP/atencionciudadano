<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;
use App\Medicamento;

class Medicamento extends Model
{
    protected $table = 'medicamentos';

    protected $fillable = ['categoria_id','name','descripcion','existencia'];

    public function Categoria()
    {
    	return $this->belongsTo(Categoria::class);
    }

     public function identificar($id)
    {
    	$name = Medicamento::find($id);
    	return $name;
    }
}
