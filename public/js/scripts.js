function desactivarCategoria(id,name) {

if (confirm('Desea eliminar: '+name+'?')){
    
	
	 $.ajax({
                type: 'DELETE',//metoodo 
                url: 'categorias/'+id,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                  //alert(data);
                  setTimeout("location.href='categorias'",1000);               
                }
            });

        }else{

            alert('Cancelado');
        }

}

function restaurar(id) {
 
     $.ajax({
                type: 'POST',//metoodo 
                url: 'restaurar/categoria/'+id,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                  //alert(data);
                  setTimeout("location.href='categorias'",1000);               
                }
            });

}

function recarga(id,name) {
  
  $('#recarga').modal('show');
  $('#medicamento').val(name);
  $('#id').val(id);
}

function desactivar(id,status) {

  //alert(id);

$.ajax({
                type: 'POST',//metoodo 
                url: '/desactivar/'+id+'/'+status,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                //alert(data);
                setTimeout("location.href='/medicamentos/create'",1000);               
                }
            });


}

function activar(id,status) {

  //alert(id);

$.ajax({
                type: 'POST',//metoodo 
                url: '/activar/'+id+'/'+status,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                //alert(data);
                setTimeout("location.href='/medicamentos/create'",1000);               
                }
            });


}

function historia(id) {
  
  //alert(id);

   $.ajax({
                type: 'POST',//metoodo 
                url: '/historia/'+id,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {


                $('#modal-secundary').modal('show');
                $('#cedula').html(data.nacionalidad+'-'+data.cedula);
                $('#nombre').html(data.nombres+' '+data.apellidos);
                $('#genero').html(data.genero);
                $('#residencia').html(data.residencia);
                $('#municipio').html(data.municipio);
                $('#telefono').html(data.telefono);
                $('#correo').html(data.correo);

                //alert(data);
                //setTimeout("location.href='/pacientes'",1000);               
                }
            });

}

function traer_med(){
    
    cat=$('#cat_m').val();
    alert(cat);


     $.ajax({
                type: 'POST',//metoodo 
                url: '/buscar/medicamentos/'+cat,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                $.each(data, function(index, val) {
                     
                    $('#medicamentos').empty(); 
                    $('#medicamentos').append("<option value='"+val.id+"'>"+val.name+" (Disponible: "+val.existencia+" blisters)</option>"); 

                });            
                }
            });
}

function buscarPaciente(){
    
    ced=$('#cedula').val();
    //alert(cat);


     $.ajax({
                type: 'POST',//metoodo 
                url: '/buscar/paciente/'+ced,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                if (data == 0) {

                $('#block').removeAttr('style');
                $('#title').html('Lo Sentimos, esta persona no esta registrada.');
                $('#content').html('No existen registros.');

                }else{

                $.each(data, function(index, val) {
                     
                $('#block').removeAttr('style');
                $('#title').html(val.nacionalidad+'-'+val.cedula+' '+val.nombres+' '+val.apellidos);
                $('#content').html('Registrado en Sistema Desde: '+val.created_at);

                $('#paciente_id').val(val.id); 

                });  

                }

                         
                }
            });



}

function suspender_paciente(id,status) {
   
$.ajax({
                type: 'POST',//metoodo 
                url: '/suspender-paciente/'+id+'/'+status,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                   setTimeout("location.href='/listado'",1000);

                }
            });

}