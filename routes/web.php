<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Auths*/
Auth::routes();

/*Login*/
Route::get('/', function () {
    return view('auth.login');
});

/*Home*/
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

/*Medicamentos*/
Route::resource('medicamentos', 'MedicamentosController')->middleware('auth');
Route::post('/desactivar/{id}/{status}', 'MedicamentosController@desactivar');
Route::post('/activar/{id}/{status}', 'MedicamentosController@activar');
Route::post('/salida/medicamento/{id}', 'MedicamentosController@salida');

/*Categorias*/
Route::resource('categorias', 'CategoriasController')->middleware('auth');
Route::post('restaurar/categoria/{id}', 'CategoriasController@restaurar');

/*Pacientes*/
Route::resource('pacientes', 'PacientesController')->middleware('auth');
Route::post('/historia/{id}', 'PacientesController@historia');
Route::get('/descargar-ficha/{id}', 'PacientesController@historiaPDF')->name('pacientes.historiasPDF');
Route::get('/listado', 'PacientesController@list')->name('pacientes.list')->middleware('auth');
Route::post('/suspender-paciente/{id}/{status}', 'PacientesController@suspender');

/**/
Route::resource('solicitudes', 'SolicitudesController')->middleware('auth');
Route::post('/buscar/paciente/{ced}', 'SolicitudesController@buscarPaciente');
Route::post('/buscar/medicamentos/{id}', 'SolicitudesController@traer_m');












