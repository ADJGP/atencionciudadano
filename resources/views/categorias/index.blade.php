@extends('layouts.admin')
@section('content')

         @if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
         @endif

<div class="row">

            <div class="col-md-4 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Crear Categoria</h4>
                  <p class="alert alert-info card-description">
                   Las categorias sirven para clasificar los medicamentos.
                  </p>
                  <form id="category" class="forms-sample" action="{{ route('categorias.store') }}" method="POST">
                  	@csrf
                    <div class="form-group">
                      <label for="exampleInputName1">Ingrese Nombre</label>
                      <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" value="{{ old('name') }}" placeholder="ingresar el nombre de la categoria...">

                      @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                      @endif

                    </div>
                    <div class="card-footer text-center">
                    <button type="submit" class="btn btn-success mr-2">Guardar</button>
                    <button class="btn btn-danger">Cancelar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

             <div class="col-lg-8 grid-margin stretch-card">
              <div class="card alert-success">
                <div class="card-body">
                  <h4 class="card-title">Categorias Disponibles</h4>
                  <p class="card-description">
                    Cada categoria tiene una coleccion de medicamentos asociada.
                  </p>
                    <table id="example1" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Categoria</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($categorias as $categoria)
                        
                         <tr>
                         	<td>{{ $loop->iteration }}</td><td>{{ $categoria->name }}</td><td><button title="Eliminar" class="btn  btn-danger" onclick="desactivarCategoria('{{ $categoria->id }}','{{ $categoria->name }}')"><i class="fa fa-close"></i></button></td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
             
           <hr>

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Categorias Desactivadas</h4>
                  <p class="card-description">
                    Estas categorias no estan disponibles para asociar con los medicamentos. Para habilitarlas oprima en "Restaurar".
                  </p>
                    <table id="example2" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Categoria</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($categoriasD as $categoria)
                        
                         <tr>
                          <td>{{ $loop->iteration }}</td><td>{{ $categoria->name }}</td><td><button title="restaurar" class="btn  btn-primary" onclick="restaurar('{{ $categoria->id }}')"><i class="fa fa-refresh"></i></button></td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
</div>
@endsection
