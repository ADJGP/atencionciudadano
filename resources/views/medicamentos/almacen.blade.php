@extends('layouts.admin')
@section('content')

         @if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
         @endif

<div class="row">
 <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Almacen de Medicamentos</h4>
                  <p class="card-description">
                    En esta seccion puedes encontrar todos los medicamentos que se han registrado en sistema.
                  </p>
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Medicamento</th>
                          <th>Categoria</th>
                          <th>Stock</th>
                          <th>Ultima Recepcion</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($medicamentos as $medicamento)
                        
                         <tr>
                         	<td>{{ $loop->iteration }}</td>
                          <td>{{ $medicamento->name }}</td>
                          <td>{{ $medicamento->Categoria->name }}</td>
                          <td>{{ $medicamento->existencia }}</td>
                          <td>{{ $medicamento->updated_at }}</td>
                          <td>
                            @if($medicamento->status==1)
                            <a class="btn btn-primary" title="Recargar Medicamento" href="{{ route('medicamentos.edit',$medicamento->id) }}" ><i class="fa fa-plus"></i></a> 
                            @else
                            <button class="btn btn-primary" disabled="disabled" title="No Disponible, Debe activar"><i class="fa fa-plus"></i></button> 
                            @endif

                            @if($medicamento->status==1) 
                            <a class="btn btn-danger" onclick="desactivar('{{$medicamento->id}}',0)" title="Desactivar Medicamento"><i class="fa  fa-thumbs-o-down"></i></a>
                            @else
                            <button class="btn btn-success" onclick="activar('{{$medicamento->id}}',1)" title="Activar Medicamento"><i class="fa  fa-thumbs-o-up"></i></button>
                            @endif

                            @if($medicamento->status==1) 
                            <a class="btn btn-warning" data-toggle='modal' data-target="#modal{{$medicamento->id}}" title="Salida de Almacen"><i class="fa  fa-mail-forward"></i></a>
                            @else
                            <button class="btn btn-warning" disabled="disabled" title="Salida de Almacen/No Disponible"><i class="fa  fa-mail-forward"></i></button>
                            @endif
                          </td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
 </div>
 </div>

    @foreach($medicamentos as $medicamento)
    <div class="modal fade" tabindex="-1" role="dialog" id="modal{{$medicamento->id}}">
        <div class="modal-dialog" role="document">
         <div class="modal-content">
         <div class="modal-header">
              <h5 class="modal-title">Salida Medicamento: {{$medicamento->name}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
         </div>
         <div class="modal-body" id="body">
         <form class="forms-sample" action="{{ action('MedicamentosController@salida',$medicamento->id) }}" method="POST">
            @csrf
                <div class="form-group">
                    <label for="exampleInputPassword1">Cantidad a Sacar:</label>
                    <input type="number" min="1" class="form-control"  name="cantidad" placeholder="1,2,3..." required="required">
               </div>
         </div>
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary">Procesar Recarga</button>
             <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
         </div>
    </form>
        </div>
       </div>
    </div>
    @endforeach
@endsection