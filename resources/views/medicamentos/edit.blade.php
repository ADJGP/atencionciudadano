@extends('layouts.admin')
@section('content')
<div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Recargar Medicamento: {{ $medicamento->name }} </h4>
                  <form class="forms-sample" action="{{ route('medicamentos.update',$medicamento->id) }}" method="POST">
                   
                   <input type="hidden" name="_method" value="PUT">
                   <input type="hidden" name="_token" value="{{ csrf_token()}}">

                    <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Categoria: </label>
                          <div class="col-sm-9">
                            <select class="form-control{{ $errors->has('categoria_id') ? ' is-invalid' : '' }}" name="categoria_id" value="{{ old('categoria_id') }}" disabled="disabled">
                              <option selected="selected" disabled="disabled">{{ $medicamento->Categoria->name }}</option>
                              @foreach($categorias as $categoria)
                              <option value="{{ $categoria->id }}">{{ $categoria->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-sm-12">
                          	@if ($errors->has('categoria_id'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('categoria_id') }}</strong>
                            </span>
                           @endif
                          </div>
                     </div>
                          
                    <div class="form-group">
                      <label for="exampleInputEmail3">Nombre del Medicamento:</label>
                      <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="exampleInputEmail3" placeholder="Nombre..." value="{{ $medicamento->name }}" disabled="disabled">
                      @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                            </span>
                           @endif
                    </div>

                     <div class="form-group">
                      <label for="exampleInputPassword4">Descripcion:</label>
                      <textarea name="descripcion" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }}" placeholder="Especifique las caracteristicas del medicamento..." disabled="disabled">{{ $medicamento->descripcion }}</textarea>
                           @if ($errors->has('descripcion'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                            </span>
                           @endif
                     </div>
                     <p class="alert alert-primary">Hey! Acontinuacion se detalla la cantidad actual que existe en Almacen.</p>
                     <div class="row">



                     <div class="form-group col-6 text-center">
                      <label for="exampleInputCity1">Cantidad en Almacen:</label>
                      <input type="number" class="form-control{{ $errors->has('existencia') ? ' is-invalid' : '' }}" id="exampleInputCity1" placeholder="Disponibilidad" value="{{ $medicamento->existencia }}" disabled="disabled">

                           @if ($errors->has('existencia'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('existencia') }}</strong>
                            </span>
                           @endif
                      </div>

                     <div class="form-group col-6 text-center">
                      <label for="exampleInputCity1">Cantidad que se añadira:</label>
                      <input name="existencia" type="number" class="form-control{{ $errors->has('existencia') ? ' is-invalid' : '' }}" id="exampleInputCity1" placeholder="Disponibilidad">

                           @if ($errors->has('existencia'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('existencia') }}</strong>
                            </span>
                           @endif
                      </div>
                     </div>
                    <div class="col-lg-12 text-center">
                    <button type="submit" class="btn btn-success mr-2">Recargar Medicamento</button>
                    <a class="btn btn-light" href="{{ route('medicamentos.create') }}">Atras</a>
                    </div>
                  </form>
                </div>
              </div>
</div>
@endsection