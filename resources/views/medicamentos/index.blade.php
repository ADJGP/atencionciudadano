@extends('layouts.admin')
@section('content')

         @if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
         @endif

<div class="row">
<div class="col-md-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Registro de Medicamentos</h4>
                  <p class="card-description alert alert-info text-justify">
                    Esta seccion permite el ingreso de medicamentos que no existen aun en el almacen de medicamentos. Antes de procesar el ingreso verifica en los medicamentos disponibles.
                  </p>
                  <form class="forms-sample" action="{{ route('medicamentos.store') }}" method="POST">
                   
                   @csrf

                    <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Categoria: </label>
                          <div class="col-sm-9">
                            <select class="form-control{{ $errors->has('categoria_id') ? ' is-invalid' : '' }}" name="categoria_id" value="{{ old('categoria_id') }}">
                              <option selected="selected" disabled="disabled">-- Seleccione --</option>
                              @foreach($categorias as $categoria)
                              <option value="{{ $categoria->id }}">{{ $categoria->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-sm-12">
                          	@if ($errors->has('categoria_id'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('categoria_id') }}</strong>
                            </span>
                           @endif
                          </div>
                     </div>
                          
                    <div class="form-group">
                      <label for="exampleInputEmail3">Nombre del Medicamento:</label>
                      <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="exampleInputEmail3" placeholder="Nombre..." value="{{ old('name') }}">
                      @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                            </span>
                           @endif
                    </div>

                     <div class="form-group">
                      <label for="exampleInputPassword4">Descripcion:</label>
                      <textarea name="descripcion" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }}" placeholder="Especifique las caracteristicas del medicamento..." value="{{ old('descripcion') }}"></textarea>
                           @if ($errors->has('descripcion'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                            </span>
                           @endif
                     </div>

                     <div class="form-group text-center">
                      <label for="exampleInputCity1">Cantidad de Ingreso</label>
                      <input name="existencia" type="number" class="form-control{{ $errors->has('existencia') ? ' is-invalid' : '' }}" id="exampleInputCity1" placeholder="Disponibilidad" value="{{ old('existencia') }}">

                           @if ($errors->has('existencia'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('existencia') }}</strong>
                            </span>
                           @endif
                      </div>

                    <div class="col-lg-12 text-center">
                    <button type="submit" class="btn btn-success mr-2">Guardar</button>
                    <button class="btn btn-light" type="reset">Cancelar</button>
                    </div>
                  </form>
                </div>
              </div>
 </div>
 <div class="col-lg-7 grid-margin stretch-card">
              <div class="card alert-success">
                <div class="card-body">
                  <h4 class="card-title">Medicamentos Disponibles</h4>
                  <p class="card-description">
                    Listado de medicamentos existentes en almacen.
                  </p>
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Medicamento</th>
                          <th>Estado</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($medicamentos as $medicamento)
                        
                         <tr>
                         	<td>{{ $loop->iteration }}</td><td>{{ $medicamento->name }}</td><td><label class="text-primary" >Dispobible</label></td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
 </div>
 </div>

@endsection