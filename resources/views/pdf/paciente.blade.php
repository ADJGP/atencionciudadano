@extends('pdf.template')
@section('content')
    <img src="{{ asset('images/logo.png') }}" width="10%" alt="logo" style="float: left;"><img src="{{ asset('images/logo.png') }}" width="10%" alt="logo" style="float: right;">
    
    <h5 class="text-center">CEDOCABAR</h5>
    <h4 class="text-center">Oficina de Atencion al Ciudadano</h4>
    <br>
    <br>
    <table class="table" style="margin-top: 2rem;">
            <tr class="success">
                <th colspan="3" class="text-center">Datos Personales</th>
            </tr>
            <tr>
                <th class="text-center">Nro. Paciente</th>
                <th class="text-center" colspan="2">Nombres y Apellidos</th>
            </tr>                            
            <tr>
                <td class="text-center" >{{ $paciente['id'] }}</td>
                <td class="text-center" colspan="2">{{ $paciente['nombres'].' '. $paciente['apellidos'] }}</td>

            </tr>
            <tr>
                <th class="text-center">Genero</th>
                <th class="text-center" colspan="2">Fecha de Nacimiento</th>
            </tr>
            <tr>
                <td class="text-center">{{ $paciente['genero'] }}</td>
                <td colspan="2" class="text-center">{{ $paciente['fec_nac'] }}</td>
            </tr>
            <tr class="success">
                <th colspan="3" class="text-center">
                    Datos de Contacto
                </th>
            </tr>
            <tr>
                <th class="text-center">Telefono de Contacto</th>
                <th class="text-center" colspan="2">Correo Electronico</th>
            </tr>
            <tr>
                <td class="text-center">{{ $paciente['telefono'] }}</td>
                <td colspan="2" class="text-center">{{ $paciente['correo'] }}</td>
            </tr>
            <tr class="success">
                <th colspan="3" class="text-center">
                    Datos de Ubicacion
                </th>
            </tr>
            <tr>
                <th class="text-center">Residencia</th>
                <th class="text-center" colspan="2">Municipio</th>
            </tr>
            <tr>
                <td class="text-center">{{ $paciente['residencia'] }}</td>
                <td colspan="2" class="text-center">{{ $paciente['municipio'] }}</td>
            </tr>   
    </table>
@endsection