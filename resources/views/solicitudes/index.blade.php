@extends('layouts.admin')
@section('content')

         @if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
         @endif

<div class="row">
<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Solicitudes Procesadas</h4>
                  <p class="card-description">
                    A continuacion se presentan todas las solicitudes procesadas en la Oficina de Atencion al Ciudadano
                  </p>
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Paciente</th>
                          <th>Estado</th>
                          <th>Historia</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($solicitudes as $solicitud)
                        
                         <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $solicitud->paciente_id }}</td>
                          <td><label class="text-primary" >Activo</label></td>
                          <td><label class="text-primary" ><button class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#modal-secundary{{ $solicitud->id }}"><i class="fa fa-list"></i></button></label></td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
 </div>
 </div>


@endsection