@extends('layouts.admin')
@section('content')

         @if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
         @endif

<div class="row">
<div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Procesar Solicitud de Medicamentos</h4>
<form class="form-sample" action="{{ route('solicitudes.store') }}" method="POST">

                    @csrf
                    <p class="alert alert-info">
                      Ingrese la cedula del solicitante, para validar que esta registrado en la Oficina de Atencion al Ciudadano.
                    </p>

                    <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Cedula</label>
                          <input type="number" min="1" maxlength="8" value="{{ old('cedula') }}" onchange="buscarPaciente()" name="cedula" id="cedula" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}">
                          @if ($errors->has('cedula'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cedula') }}</strong>
                                </span>
                               @endif
                        </div>
                        <div class="form-group col-md-6">
                         
                        </div>
                        <div class="form-group col-md-6" id="block" style="display: none" >
                          <h4 id="title">V-23520344 Alexis Gonzalez</h4>
                          <p id="content">Registrado desde 01/02/2019</p>
                        </div>
                        <input type="hidden" name="paciente_id" id="paciente_id">
                    </div>
                </div>
              </div>
</div>
  <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Cargue los medicamentos a procesar.</h4>
                       <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Categorias de Medicamentos</label>
                          <select name="cat_m" id="cat_m" class="form-control" onchange="traer_med()">
                                 <optgroup>
                                         <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                         @foreach($categorias as $cat)
                                         <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                         @endforeach
                                 </optgroup>
                          </select>

                              @if ($errors->has('cat_m'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cat_m') }}</strong>
                                </span>
                              @endif
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Medicamentos Disponibles</label>
                         <select name="medicamento_id" id="medicamento_id" class="form-control">

                                    <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                 <optgroup id="medicamentos">
                                         
                                 </optgroup>
                          </select>

                                @if ($errors->has('medicamento_id'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('medicamento_id') }}</strong>
                                </span>
                               @endif
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Cantidad que se entrega:</label>
                          <input type="number" min="1" value="{{ old('cantidad_e') }}" name="cantidad_e" id="cantidad_e" class="form-control{{ $errors->has('cantidad_e') ? ' is-invalid' : '' }}">

                              @if ($errors->has('cantidad_e'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cantidad_e') }}</strong>
                                </span>
                               @endif
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Detalle de la Entrega</label>
                          <input type="text" value="{{ old('detalle') }}" name="detalle" id="detalle" class="form-control{{ $errors->has('detalle') ? ' is-invalid' : '' }}">

                              @if ($errors->has('detalle'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('detalle') }}</strong>
                                </span>
                               @endif
                        </div>     
                      </div>          
                  </div>
                  <div class="card-footer col-md-12 text-center">

                          <button type="submit" class="btn btn-primary">Procesar</button>
                                
                  </div>
                </div>
              </div>
</div>
</form>

 <div class="modal modal-default fade" id="modal-secundary">
          <div class="modal-dialog">
            <div class="modal-content" >
              <div class="modal-header">
                <h4>Lista de Medicamentos:</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body" id="text">
                <div class="card">
                   <div class="card-body">
                    <table class="table table-striped">
                      
                    </table>
                   </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
  </div>
@endsection