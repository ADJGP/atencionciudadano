<footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © {{ date('Y') }}
              <a href="http://www.bootstrapdash.com/" target="_blank">CEDOCABAR</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Equipo de Informatica de RRHH-Iluminatis Group 
              <i class="fa fa-rebel text-danger"></i>
            </span>
          </div>
        </footer>