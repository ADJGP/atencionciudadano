@extends('layouts.admin')
@section('content')

@if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
@endif

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Listado de Pacientes</h4>
                  <p class="card-description">
                    Listado de Pacientes inscritos en la Oficina de Atencion al Ciudadano.
                  </p>
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Identificacion</th>
                          <th>Paciente</th>
                          <th>Estado</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($pacientes as $paciente)
                        
                         <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $paciente->nacionalidad.'-'.$paciente->cedula }}</td>
                          <td>{{ $paciente->nombres.' '.$paciente->apellidos }}</td>
                          <td>
                            @if($paciente->status == 0)
                            <label class="text-primary" >Activo</label></td>
                            @else
                            <label class="text-primary" >Desactivado</label></td>
                            @endif
                          <td>
                            <a class="btn btn-info btn-sm" href="{{ route('pacientes.show',$paciente->id) }}" title="Historia del Paciente"><i class="fa fa-vcard"></i></a>

                            @if($paciente->status == 0) 

                            <button class="btn btn-danger btn-sm" title="Suspender Paciente" onclick="suspender_paciente('{{ $paciente->id }}',1)"><i class="fa fa-exclamation-triangle"></i></button>

                            @else

                            <button class="btn btn-success btn-sm" title="Activar Paciente" onclick="suspender_paciente('{{ $paciente->id }}',0)"><i class="fa fa-check"></i></button>

                            @endif
                          </td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
        </div>
 </div>
@endsection