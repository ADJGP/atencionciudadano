@extends('layouts.admin')
@section('content')
<div class="col-md-12 text-left mb-3">
                  	<a class="btn btn-light btn-outline-dark" href="{{ route('pacientes.list') }}">Atras</a>
</div>
<div class="col-md-12 grid-margin stretch-card">

                  <div class="card">
                    <div class="card-body">
                      <blockquote class="blockquote blockquote-primary">
                    <h4 class="text-center">{{ $paciente->nacionalidad.'-'.$paciente->cedula.' '.$paciente->nombres.' '.$paciente->apellidos }}</h4>
                    <footer class="blockquote-footer text-center">Paciente inscrito en la Oficina de Atencion al Ciudadano desde {{ $paciente->created_at }}.
                    </footer>
                  </blockquote>
                      <h3 class="card-title text-center"></h3>
                      <p class="card-description text-center">
                        
                      </p>
                      
                      <h4 class="text-center mb-3">Informacion Personal</h4>
                      <table class="table table-hover table-primary mb-3">
                      		<tr>
                      			<th>Telefono:</th>
                      			<td>{{ $paciente->telefono }}</td>
                      			<th>Correo:</th>
                      			<td>{{ $paciente->correo }}</td>
                      		</tr>
                      		<tr>
                      			<th>Municipio:</th>
                      			<td>{{ $paciente->municipio }}</td>
                      			<th>Residencia:</th>
                      			<td>{{ $paciente->residencia }}</td>
                      		</tr>
                      </table>

                      <h4 class="text-center mb-3">Solicitudes Procesadas</h4>

                      <table class="table table-hover table-success mb-3">
                      		
                      			<th>#</th>
                            <th>Fecha de Solicitud</th>
                            <th>Medicamento Solicitado</th>
                      			<th>Cantidad Solicitada</th>
                      			<th>Detalle</th>
                      			<th>Status</th>
                      		
                      		<tbody>
                      			@foreach($paciente->Solicitudes as $sol)
                      			<tr>
                      				<td>{{ $loop->iteration }}</td>
                              <td>{{ $sol->created_at }}</td>
                              <td>{{ $sol->buscarMedicamento($sol->medicamento_id) }}</td>
                      				<td>{{ $sol->cantidad }}</td>
                      				<td>{{ $sol->detalle }}</td>
                      				@if($sol->status == 1)

                      				<td><span class="badge badge-primary">Procesada</span></td>

                      				@else
                                     
                                    <td><span class="badge badge-danger">No Aprobada</span></td>

                      				@endif
                      			</tr>
                      			@endforeach
                      		</tbody>
                      </table>
                    </div>
                  </div>
                  
</div>
@endsection