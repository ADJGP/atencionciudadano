@extends('layouts.admin')
@section('content')

         @if (session('message'))
          <div class="row purchace-popup">
            <div class="col-12">
              <p class="alert alert-{{ session('class') }} card-description">
                   {{ session('message') }}</p>
            </div>
          </div>
         @endif

<div class="row">
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Registro de Pacientes</h4>
                  <form class="form-sample" action="{{ route('pacientes.store') }}" method="POST">

                    @csrf
                    <p class="alert alert-info">
                      Por favor ingresar la informacion personal del paciente.
                    </p>

                    <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Nacionalidad</label>
                          <select class="form-control{{ $errors->has('nacionalidad') ? ' is-invalid' : '' }}" name='nacionalidad' >
                                  <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                  <option value="V" {{ old('nacionalidad') == 'V' ? 'selected' : '' }}>V</option>
                                  <option value="E" {{ old('nacionalidad') == 'E' ? 'selected' : '' }}>E</option>
                          </select>
                          @if ($errors->has('nacionalidad'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nacionalidad') }}</strong>
                                </span>
                               @endif

                          
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Cedula</label>
                          <input type="number" min="1" maxlength="8" value="{{ old('cedula') }}" name="cedula" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}">
                          @if ($errors->has('cedula'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cedula') }}</strong>
                                </span>
                               @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Nombres</label>
                          <input type="text" name="nombres" value="{{ old('nombres') }}" class="form-control{{ $errors->has('nombres') ? ' is-invalid' : '' }}">
                          @if ($errors->has('nombres'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombres') }}</strong>
                                </span>
                               @endif

                          
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Apellidos</label>
                          <input type="text" name="apellidos" value="{{ old('apellidos') }}" class="form-control{{ $errors->has('apellidos') ? ' is-invalid' : '' }}">
                          @if ($errors->has('apellidos'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellidos') }}</strong>
                                </span>
                               @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Genero</label>
                          <select class="form-control{{ $errors->has('genero') ? ' is-invalid' : '' }}"  name="genero">
                              <option selected="selected" disabled="disabled">-- Seleccione --</option>

                              <option value="M" {{ old('genero') == 'M' ? 'selected' : '' }}>Masculino</option>
                              <option value="F" {{ old('genero') == 'F' ? 'selected' : '' }}>Femenino</option>

                            </select>
                          @if ($errors->has('genero'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('genero') }}</strong>
                                </span>
                               @endif

                          
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Fecha de Nacimiento</label>
                          <input type="date" name="fec_nac" value="{{ old('fec_nac') }}" class="form-control{{ $errors->has('fec_nac') ? ' is-invalid' : '' }}">
                          @if ($errors->has('fec_nac'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('fec_nac') }}</strong>
                                </span>
                               @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Telefono</label>
                          <input type="number" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}"  min="11" placeholder="04100000000" value="{{ old('telefono') }}" name="telefono">
                          @if ($errors->has('genero'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('genero') }}</strong>
                                </span>
                               @endif

                          
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Correo Electronico</label>
                          <input type="email" value="{{ old('correo') }}" class="form-control{{ $errors->has('correo') ? ' is-invalid' : '' }}"  name="correo">
                          @if ($errors->has('correo'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('correo') }}</strong>
                                </span>
                               @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Residencia</label>
                          <input type="text" name="residencia" value="{{ old('residencia') }}" class="form-control{{ $errors->has('residencia') ? ' is-invalid' : '' }}">
                          @if ($errors->has('residencia'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('residencia') }}</strong>
                                </span>
                               @endif

                          
                        </div>
                        <div class="form-group col-md-6">
                          <label for="exampleInputName1">Municipio</label>
                          <input type="text" name="municipio" value="{{ old('municipio') }}" class="form-control{{ $errors->has('municipio') ? ' is-invalid' : '' }}">
                          @if ($errors->has('municipio'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('municipio') }}</strong>
                                </span>
                               @endif
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success mr-2">Guardar</button>
                    <button class="btn btn-light">Cancelar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

<div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Pacientes Registrados</h4>
                  <p class="card-description">
                    Listado de Pacientes inscritos en la Oficina de Atencion al Ciudadano.
                  </p>
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nro</th>
                          <th>Paciente</th>
                          <th>Estado</th>
                          <th>Historia</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($pacientes as $paciente)
                        
                         <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $paciente->nombres }}</td>
                          <td><label class="text-primary" >Activo</label></td>
                          <td><label class="text-primary" ><button class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#modal-secundary{{ $paciente->id }}"><i class="fa fa-list"></i></button></label></td>
                         </tr>

                        @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
 </div>
 </div>

 @foreach($pacientes as $paciente)
 <div class="modal modal-default fade" id="modal-secundary{{ $paciente->id }}">
          <div class="modal-dialog">
            <div class="modal-content" >
              <div class="modal-header">
                <h4>Ficha del Paciente: {{ $paciente->nombres.' '.$paciente->apellidos }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body" id="text">
                <div class="card">
                   <div class="card-body">
                <table class="table table-hover table-striped">
                  <tr class="text-center"><th colspan="2">Datos Personales</th></tr>
                  <tr><td>Identificacion: </td><td id="cedula">{{ $paciente->nacionalidad.'-'.$paciente->cedula }}</td></tr>
                  <tr><td>Nombre: </td><td id="nombre">{{ $paciente->nombres.' '.$paciente->apellidos }}</td></tr>
                  <tr><td>Sexo: </td><td id="genero">{{ $paciente->genero }}</td></tr>
                  <tr class="text-center"><th colspan="2">Datos de Ubicacion</th></tr>
                  <tr><td>Residencia: </td><td id="residencia">{{ $paciente->residencia }}</td></tr>
                  <tr><td>Municipio: </td><td id="municipio">{{ $paciente->municipio }}</td></tr>
                  <tr class="text-center"><th colspan="2">Datos de Contacto</th></tr>
                  <tr><td>Telefono: </td><td id="telefono">{{ $paciente->telefono }}</td></tr>
                  <tr><td>Correo: </td><td id="correo">{{ $paciente->correo }}</td></tr>
                </table>
                </div>
                </div>
              </div>
              <div class="modal-footer">
                <a href="{{ route('pacientes.historiasPDF',$paciente->id) }}" class="btn btn-primary btn-outline-dark">PDF</a>
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
  </div>
        <!-- /.modal --> 
@endforeach

@endsection